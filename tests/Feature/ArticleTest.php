<?php

namespace Tests\Feature;

use App\Models\Article;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic feature test example.
     *
     * @return void
     * @test
     */
    public function articleCreate()
    {
        $create = Article::factory()->make();

        $response =  $this->post(route('panel.article.create'), 
            ['title' => $create->title]
        );

        $this->assertDatabaseHas('articles', 
            ['title' => $create->title]
        );

        $response->assertStatus(302);
    }

    
    /**
     * @return void
     * @test
     */
    public function articleContentUpdate()
    {
        $create = Article::factory()->create();

        $response = $this->put(
            route('panel.article.update.content', $create->article_code), 
            [
                'content' => '<div>updated test</div>',
            ]
        );

        $response->assertStatus(200);
    }

     /**
     * @return void
     * @test
     */
    public function articleUpdate()
    {
        $create = Article::factory()->create();

        $this->put(
            route('panel.article.update', ),
            [
                
            ]
        );
    }
}
